import QtQuick
import QtQuick.Window
import QtQuick.Controls
import FluentUI
import "./page"
import "./window"
import "./component"

Window {
    property string name
    id: mainWindow
    width: SCREEN_WIDTH //1140
    height: SCREEN_HEIGHT  //800
    minimumWidth: 628
    minimumHeight: 628
    visible: true
    opacity: 1
    color: "#F2F3F4"
    LoginPage {
        id: welcomeItem
        anchors.fill: parent
    }
    PrivacyPage {
        id: loginItem
        visible: false
        anchors.fill: parent
    }

    Item {
        id: mainItem
        visible: false
        anchors.fill: parent

        RecExpander {
            x: 44
            y: 188
        }

        Text {
            id: mindText
            width: 216
            height: 49
            x: 44
            y: 116
            color: "#333333"
            font.pixelSize: 36
            font.weight: 400
            text: "Mind 2023"
            font.family: "MicrosoftYaHei"
        }

        Image {
            id: userImage
            width: 42
            height: 42
            x: 1063
            y: 116
            asynchronous: true
            cache: true
            fillMode: Image.PreserveAspectFit
            source: "Resources/user.png"
        }

        Image {
            id: edgeImage
            width: 500
            height: 500
            x: 610
            y: 161
            asynchronous: true
            cache: true
            fillMode: Image.PreserveAspectFit
            source: "Resources/edge.png"
        }

        Rectangle {
            x: 0
            y: 680
            anchors.bottom: parent.bottom
            width: parent.width
            height: 115
            color: "#D3DBDF"
            border.color: "#B1B1B1"
        }
    }

    PropertyAnimation {
        running: true
        target: welcomeItem
        property: 'visible'
        to: false
        duration: 1000
    }
    Timer {
        interval: 1000
        onTriggered: {
            loginItem.visible = true
        }
        running: true
    }
}
