import QtQuick 2.15

Item {
    property bool isExpander: false
    property string deviceName
    property string productType
    property string serialNumber
    property string processor
    property string gpuType
    property string memorySize
    property string storageSize
    property string biosVersion
    property string operatingSystemType
    function changeExpander() {
        getDeviceInfo();
        inforImage.rotation += 180
        if (!isExpander)
        {
            isExpandAnimation.running = true;
            typeText.visible = true;
            infosText.visible = true;
            isExpander = true;
        } else {
            expandAnimation.running = true;
            isExpander = false;
            typeText.visible = false;
            infosText.visible = false;
        }
    }
    function getDeviceInfo() {
        deviceName = deviceInfos.getDeviceName();
        productType = deviceInfos.getProductType();
        serialNumber = deviceInfos.getSerialNumber();
        processor = deviceInfos.getProcessor();
        gpuType = deviceInfos.getGpuType();
        memorySize = deviceInfos.getMemorySize();
        storageSize = deviceInfos.getStorageSize();
        biosVersion = deviceInfos.getBiosVersion();
        operatingSystemType = deviceInfos.getOperatingSystemType();
    }

    Rectangle {
        id: infoRec
        color: "#FCFBFB"
        width: 542
        height: 85
        radius: [5, 5, 0, 0]
        border.width: 1
        border.color: "#E8E6E2"
        Image {
            id: infoImage
            width: 28
            height: 28
            anchors.left: parent.left
            anchors.leftMargin: 26
            anchors.top: parent.top
            anchors.topMargin: 32
            asynchronous: true
            cache: true
            fillMode: Image.PreserveAspectFit
            source: "../Resources/infor.png"
        }
        Image {
            id: inforImage
            width: 11.95
            height: 6
            anchors.left: parent.left
            anchors.leftMargin: 503
            anchors.top: parent.top
            anchors.topMargin: 40
            asynchronous: true
            cache: true
            fillMode: Image.PreserveAspectFit
            source: "../Resources/path.png"
        }
        Text {
            id: infoText
            width: 98
            height: 22
            anchors.left: parent.left
            anchors.leftMargin: 77
            anchors.top: parent.top
            anchors.topMargin: 32
            color: "#333333"
            font.pixelSize: 18
            font.weight: 400
            font.family: "MicrosoftYaHei"
            text: qsTr("设备信息")
        }
        MouseArea {
            anchors.fill: infoRec
            onClicked: {
                changeExpander();
            }
        }
    }
    Rectangle {
        id: infoExpander
        radius: [0, 0, 5, 5]
        color: "#FCFBFB"
        anchors.top: infoRec.bottom
        width: 542
        height: 0
        border.width: 1
        border.color: "#E8E6E2"
        Text {
            id: typeText
            visible: false
            width: 136
            height: 262
            anchors {
                left: parent.left
                leftMargin: 77
                top: parent.top
                topMargin: 22
            }
            color: "#333333"
            font.family: "MicrosoftYaHei"
            font.weight: 400
            lineHeight: 1.4
            font.pixelSize: 18
            text: qsTr("设备名称\n产品型号\n序列号\n处理器\nGPU\n内存\n存储空间\nBIOS版本\n操作系统版本")
        }
        Text {
            id: infosText
            visible: false
            width: 136
            height: 262
            anchors {
                left: typeText.right
                leftMargin: 16
                top: parent.top
                topMargin: 22
            }
            color: "#7E7E7E"
            font.family: "MicrosoftYaHei"
            font.weight: 400
            lineHeight: 1.4
            font.pixelSize: 18
            text: qsTr(deviceName + "\n" + productType + "\n" + serialNumber + "\n"
                       + processor + "\n" + gpuType + "\n" + memorySize + "\n" + storageSize + "\n"
                       + biosVersion + "\n" + operatingSystemType)
        }
    }

    PropertyAnimation {
        id: expandAnimation
        target: infoExpander
        properties: "height"
        to: 0
        duration: 100
    }

    PropertyAnimation {
        id: isExpandAnimation
        target: infoExpander
        properties: "height"
        to: 316
        duration: 100
    }
}
