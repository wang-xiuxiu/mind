#include "deviceinfos.h"
#include <QDateTime>

DeviceInfos::DeviceInfos()
{

}

QString DeviceInfos::getDeviceName()
{
//    QTime current_time = QTime::currentTime();
//    QString time = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz");

    return "USERMIND";
}

QString DeviceInfos::getProductType()
{
    return "Mind 2.0";
}

QString DeviceInfos::getSerialNumber()
{
    return "XX-XXXX-XXX";
}

QString DeviceInfos::getProcessor()
{
    return "intel i7-13900KH";
}

QString DeviceInfos::getGpuType()
{
    return "RTX-4060";
}

QString DeviceInfos::getMemorySize()
{
    return "16GB";
}

QString DeviceInfos::getStorageSize()
{
    return "512GB(320G可用)";
}

QString DeviceInfos::getBiosVersion()
{
    return "1.0.0.1";
}

QString DeviceInfos::getOperatingSystemType()
{
    return "Windows 11 Home";
}


