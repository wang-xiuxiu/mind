#ifndef DEVICEINFOS_H
#define DEVICEINFOS_H

#include <QObject>

#include <QtCore/qstring.h>

class DeviceInfos: public QObject
{
    Q_OBJECT
public:
    DeviceInfos();

public slots:
    QString getDeviceName();
    QString getProductType();
    QString getSerialNumber();
    QString getProcessor();
    QString getGpuType();
    QString getMemorySize();
    QString getStorageSize();
    QString getBiosVersion();
    QString getOperatingSystemType();
};

#endif // DEVICEINFOS_H
