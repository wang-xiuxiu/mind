#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QScreen>
#include <QtNetWork>
#include "deviceinfos.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    DeviceInfos deviceInfos;
    QQmlApplicationEngine engine;
    QNetworkAccessManager managerNet;
    //全局上下文  上下文对象
    QQmlContext* context = engine.rootContext();
    context->setContextProperty("SCREEN_WIDTH", "1140");
    context->setContextProperty("SCREEN_HEIGHT", "800");
    engine.rootContext()->setContextProperty("deviceInfos",&deviceInfos);

    const QUrl url(u"qrc:/Mind/Main.qml"_qs);
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreationFailed,
        &app, []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
