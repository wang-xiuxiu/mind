import QtQuick 2.15
import FluentUI

Item {
    Image {
        id: loginImage
        height: mainWindow.height/4.24
        width: mainWindow.width/3.8
        x: mainWindow.width/2.71
        y: mainWindow.height/3.21
        asynchronous: true
        cache: true
        fillMode: Image.PreserveAspectFit
        source: "../Resources/Login.png"
        anchors.centerIn: parent
    }
    Text {
        height: mainWindow.height/16.67
        width: mainWindow.width/3.99
        x: mainWindow.width/2.66
        y: mainWindow.height/1.48
        color: "#333333"
        font.pixelSize: mainWindow.width/31.67
        font.weight: 400
        font.letterSpacing: 0.2
        text: "欢 迎 使 用 Mind"
        font.family: "MicrosoftYaHei"
    }
}
