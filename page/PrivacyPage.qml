import QtQuick 2.15
import QtQuick.Window
import QtQuick.Controls
import FluentUI

Item {
    id: loginItem
    visible: false
    anchors.fill: parent
    Text {
        id: textOne
        height: mainWindow.height/16.67
        width: mainWindow.width/3.17
        x: mainWindow.width/3.04
        y: mainWindow.height/3.98
        color: "#333333"
        font.pixelSize: mainWindow.width/31.67
        font.weight: 400
        text: "选择您设备的隐私设置"
        font.family: "MicrosoftYaHei"
    }
    Text {
        height: mainWindow.height/25
        width: mainWindow.width/2.92
        x: mainWindow.width/3.04
        y: mainWindow.height/3.14
        color: "#333333"
        font.pixelSize: mainWindow.width/47.5
        font.weight: 400
        text: "Mind允许您对您的隐私进行控制。"
        font.family: "MicrosoftYaHei"
    }
    Text {
        height: mainWindow.height/3.19
        width: mainWindow.width/15.62
        x: mainWindow.width/5.56
        y: mainWindow.height/2.26
        text: "Mind 应用程序使用定位服务通过\"查找我的设备\"追踪设备的位置。如需打开关闭此功能，\n请在下列设置中修改位置权限设置"
        font.pixelSize: mainWindow.width/63.33
        font.weight: 400
        font.family: "MicrosoftYaHei"
        color: "#7E7E7E"
        font.letterSpacing: 0.0
    }

    CheckBox {
        id: locaCheckBox
        text: qsTr("位置权限已打开")
        //        checked: true
        x:mainWindow.width/4.87
        y:mainWindow.height/1.86
        spacing: 0
        font.pointSize: mainWindow.width/95
        //选中指示图片
        indicator:Rectangle{
            width: mainWindow.width/57
            y:mainWindow.width/126.67
            Image {
                height: mainWindow.width/76
                width: mainWindow.width/76
                source: locaCheckBox.checked ? "../Resources/LocationChecked.png" : "../Resources/LocationUnChecked.png"
            }
        }
        //文本
        contentItem: Text {
            text: locaCheckBox.checked ? "位置权限已打开" : "位置权限已关闭"
            font: locaCheckBox.font //字体大小
            leftPadding: locaCheckBox.indicator.width + locaCheckBox.spacing
        }
    }

    Rectangle {
        id: beginRec
        width: mainWindow.width/8.32
        height: mainWindow.height/21.05
        anchors.left: parent.left
        anchors.leftMargin: mainWindow.width/2.32
        anchors.top: parent.top
        anchors.topMargin: mainWindow.height/1.32
        //        x: mainWindow.width/2.27
        //        y: mainWindow.height/1.32
        color: "#1E95D4"
        radius: [5, 5, 5, 5]
        Text {
            text: "开始"
            color: "#FFFFFF"
            anchors.centerIn: parent
            font.pixelSize: mainWindow.width/63.33
            font.weight: 400
            font.family: "MicrosoftYaHei"
        }
        MouseArea {
            anchors.fill: parent
            onClicked:  {
                loginItem.visible = false
                mainItem.visible = true
            }
        }
    }

    CheckBox {
        id: dataCheckBox
        text: qsTr("向开发者发送可选的诊断数据")
        //        checked: true
        x: mainWindow.width/2.6697
        y: mainWindow.height/1.5656
        spacing: 0
        font.pointSize: mainWindow.width/95
        //选中指示图片
        indicator:Rectangle{
            width: mainWindow.width/57
            y:mainWindow.width/126.67
            Image {
                height: mainWindow.width/76
                width: mainWindow.width/76
                source: dataCheckBox.checked ? "../Resources/DataSend.png" : "../Resources/DataUnSend.png"
            }
        }
        //文本
        contentItem: Text {
            text: dataCheckBox.text
            font: dataCheckBox.font //字体大小
            leftPadding: dataCheckBox.indicator.width + dataCheckBox.spacing
        }
    }


    Text {
        id: privacyText
        height: mainWindow.height/34.78
        width: mainWindow.width/10.27
        x: mainWindow.width/2.18
        y: mainWindow.height/1.43
        text: "隐私声明"
        font.pixelSize: mainWindow.width/63.33
        font.weight: 400
        font.family: "MicrosoftYaHei"
        color: "#1E95D4"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                privacyPopup.open()
            }
        }
    }

    Popup{
        id: privacyPopup
        x: mainWindow.width/5.56
        y: mainWindow.height/10.13
        width: mainWindow.width/1.56
        height: mainWindow.height/1.1
        closePolicy: "NoAutoClose"
        background: Rectangle{
            radius: 10
        }
        enter: Transition {
            NumberAnimation { property: "opacity"; from: 0.0; to: 1.0 ;duration: 300}
        }
        exit: Transition {
            NumberAnimation { property: "opacity"; from: 1.0; to: 0.0 ;duration: 300}
        }

        Rectangle{
            x: privacyPopup.width/2.49
            y: privacyPopup.height/7.17
            width: privacyPopup.width/5.08
            height: privacyPopup.height/15.23
            Text{
                font.weight: 400
                font.family: "MicrosoftYaHei"
                font.pixelSize: mainWindow.width/31.667
                text: "隐私声明"
            }
        }
        Rectangle{
            id: rec
            x: privacyPopup.width/11.25
            y: privacyPopup.height/4.02
            width: privacyPopup.width/1.22
            height: privacyPopup.height/1.75
            Text{
                anchors.fill: rec
                lineHeight: 1.5
                font.weight: 400
                font.family: "MicrosoftYaHei"
                font.pixelSize: mainWindow.width/76
                wrapMode: Text.WordWrap
                text:qsTr("Khadas会收集你向我们提供的数据、你与 Khadas Mind 之间的互动数据以及产品使用数据。 部分数据由你直接提供，其他数据则由我们通过你与产品的交互以及对产品的使用和体验收集而来。我们收集的数据取决于你与 Khadas Mind 互动的环境、你所做的选择，包括你的隐私设置以及你所使用的产品和功能。我们还从第三方获取有关你的数据。\n\n对于你使用的技术和共享的数据，你可以做出不同的选择。 当我们请求你提供个人数据时，你可以拒绝。我们的很多产品都需要你提供某些个人数据，以便向你提供服务。 如果选择不提供为你提供某个产品或功能所需的数据，无法使用该产品或功能。同样，如果我们需要依法收集个人数据，或者需要与你签订合同或对你履行合同，但你未提供数据，我们将无法签订合同；或者如果这涉及到你正在使用的现有产品，我们可能必须暂停或取消你的使用权。 如果属于上述情况，我们将通知你。 在不强制要求提供数据的情况下，如果你选择不共享个人数据，那么你将无法使用需要提供此类数据的功能（如个性化功能）。")
            }
        }

        Button{
            x: privacyPopup.width/2.47
            y: privacyPopup.height/1.16
            width: privacyPopup.width/5.54
            height: privacyPopup.height/19.24
            hoverEnabled: false
            background: Rectangle{
                color: "#1E95D4"
                radius: 5;
                Text {
                    text: "确定"
                    color: "#FFFFFF"
                    anchors.centerIn: parent
                    font.weight: 400
                    font.family: "MicrosoftYaHei"
                    font.pixelSize: privacyPopup.width/42.833
                }
            }
            onClicked: {
                privacyPopup.close()
            }
        }

        Image {
            x: privacyPopup.width/1.04
            y: privacyPopup.height/60.92
            width: privacyPopup.width/63.95
            height: privacyPopup.height/63.95
            source: "../Resources/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    privacyPopup.close()
                }
            }
        }
    }
}
